﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=392286
(function () {
    "use strict";
   
    var app = WinJS.Application;
    var activation = Windows.ApplicationModel.Activation;
    var menu = new Windows.UI.Popups.PopupMenu();
 
    app.onactivated = function (args) {
        if (args.detail.kind === activation.ActivationKind.launch) {
            if (args.detail.previousExecutionState !== activation.ApplicationExecutionState.terminated) {
                // TODO: This application has been newly launched. Initialize
                // your application here.
              
            } else {
                // TODO: This application has been reactivated from suspension.
                // Restore application state here.
            }

            args.setPromise(WinJS.UI.processAll());
            //hide loader on start
            $("progress").hide();
            $("#copy-btn").hide();

            WinJS.UI.Pages.define("default.html", {
                ready: function (element, options) {
                    document.getElementById("fileValidator").addEventListener("click", fileUploadClickHandler, false);
                    document.getElementById("validator").addEventListener("click", buttonClickHandler, false);
                    if (document.getElementById("copy-btn")) {
                        document.getElementById("response").addEventListener("contextmenu", clipboardHandler, false);
                        document.getElementById("copy-btn").addEventListener("click", copyToClipboard, false);
                        document.querySelector("#cmdValidate").addEventListener("click", buttonClickHandler, false);
                        document.querySelector("#cmdValidateFile").addEventListener("click", fileUploadClickHandler, false);
                        document.querySelector("#cmdClear").addEventListener("click", function (e) {
                            $("#response").empty();
                        }, false);
                        document.querySelector("#cmdCopy").addEventListener("click", copyToClipboard, false);
                    }
                }
            });

            
        }
    };

    app.oncheckpoint = function (args) {
        // TODO: This application is about to be suspended. Save any state
        // that needs to persist across suspensions here. You might use the
        // WinJS.Application.sessionState object, which is automatically
        // saved and restored across suspension. If you need to complete an
        // asynchronous operation before your application is suspended, call
        // args.setPromise().
    };

    app.start();

    function fileUploadClickHandler(eventInfo) {
        var openPicker = new Windows.Storage.Pickers.FileOpenPicker();
        openPicker.suggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.documentsLibrary;
        openPicker.viewMode =
        Windows.Storage.Pickers.PickerViewMode.list;
        openPicker.fileTypeFilter.clear();
        openPicker.fileTypeFilter.append(".html");
        openPicker.fileTypeFilter.append(".htm");
        openPicker.fileTypeFilter.append(".xhtml");
        try{
            openPicker.pickSingleFileAsync().done(function (file) {
                if (file != null) {
                    $("#filePath").val(file.path);
                    file.openReadAsync().done(function (stream) {

                        var inputStream = stream.getInputStreamAt(0);
                        var reader = new Windows.Storage.Streams.DataReader(inputStream);
                        var size = stream.size;
                        if (size > 0) {
                            reader.loadAsync(size).then(function () {
                                var array = new Array(size);
                                reader.readBytes(array);

                                var newString = "";
                                for (var i = 0; i < array.length; i++) {
                                    // only printable characters (include spaces because could be part of names) (very rough here)

                                    if (array[i] >= 32 && array[i] <= 126) {
                                        var c = String.fromCharCode(array[i]);
                                        newString += c;
                                    }
                                }

                                $("#uri, #validator, label").hide();
                                //clear last action 
                                $("#response").empty();
                                //set ajax cache to false - alows user to valdiate page multiple times
                                $.ajaxSetup({ cache: false });
                                //get user url input
                                var selectedURL = $("#uri").val();
                                //show loading
                                $("progress").show();
                                $("#copy-btn").show();
                                $("#response").append("<p>File: <i>" + file.path + "</i></p>")

                                var url = "https://validator.w3.org/check?output=json&fragment=" + newString;
                               
                                ajaxRequest(url);
                            });
                        }
                    });
                }
                });
    }catch(err)
    {
        $("#response").append("File not opened!");
        console.log(err);
    }
    }
           
            //method:: by matthew de marillac
            //validates a url's HTML doc from W3C JSON API
            //uses JQuery
            function buttonClickHandler(eventInfo) {
                try {
                    //hide inputs to prevent double call to this method
                    $("#uri, #validator, label").hide();
                    //clear last action 
                    $("#response").empty();
                    //set ajax cache to false - alows user to valdiate page multiple times
                    $.ajaxSetup({ cache: false });
                    //get user url input
                    var selectedURL = $("#uri").val();
                    //API url
                    var url = "https://validator.w3.org/check?output=json&uri=" + selectedURL;
                    //show loading
                    $("progress").show();
                    $("#copy-btn").show();
                    //get JSON from API
                    ajaxRequest(url);
                } catch (exception) {
                    WinJS.log && WinJS.log("Error: " + exception);
                    $("#response").append(window.toStaticHTML(+"Sorry there was an unexpected error! Details:<br/>" + exception));
                    $("progress").hide();
                    $("#uri, #validator, label").show();
                    return;
                }
            }
 
            function ajaxRequest(url)
            {
                var api = $.getJSON(url,
                         function (data) {
                             WinJS.log && WinJS.log("AJAX request made");
                             if (data != undefined) {
                                 //header output
                                 //if validation errors - show message and errors
                                 if (data.messages.length > 0) {
                                     $("#response").append(window.toStaticHTML("<h3 style='color:#ff0000;'>Validation Output: ") +
                                         data.messages.length +
                                         window.toStaticHTML(" messages" + "</h3><br/>"));
                                     WinJS.log && WinJS.log("Validation Success! with markup errors");
                                 } else {
                                     //document is valid HTML- display success message and exit
                                     $("#response").append(window.toStaticHTML("<h3 style='color:#00ff00;'>Document is valid HTML</h3>"));
                                     $("progress").hide();
                                     $("#uri, #validator, label").show();
                                     WinJS.log && WinJS.log("Validation Success! fully validated");
                                     return;
                                 }
                                 //foreach error or warning print html reponse
                                 $.each(data.messages, function (i, item) {
                                     if (data.messages != undefined) {
                                         //show error type icons
                                         if (item.type == "error") {
                                             $("#response").append("<img class='icon' src='error.png'>");
                                         } else {
                                             $("#response").append("<img class='icon' src='warning.png'>");
                                         }
                                         //show line and column error appears
                                         if (item.type == "error") {
                                             $("#response").append(window.toStaticHTML("<i>Line ") + item.lastLine +
                                                 window.toStaticHTML(", Column " + item.lastColumn + ": </i>"));
                                         }
                                         //print error message
                                         $("#response").append(window.toStaticHTML(item.message + "<br/><br/>"));
                                     }
                                 })
                             }
                             //end loading
                             $("progress").hide();
                             $("#uri, #validator, label").show();
                             return;
                             //if ajax failed
                         }).fail(function (jqXHR, textStatus, errorThrown) {
                             //end loading and display error
                             $("#response").append(window.toStaticHTML("<p>This webpage is either unavailable" +
                                 " or cannot be connected to at this moment.</p>"));
                             WinJS.log && WinJS.log("AJAX request failed- error: " + errorThrown);
                             $("progress").hide();
                             $("#uri, #validator, label").show();
                             return;
                         });
            }

            //custom copy button/context operation logic

            // Converts from page to WinRT coordinates, which take scale factor into consideration.
            function pageToWinRT(pageX, pageY) {
                var zoomFactor = document.documentElement.msContentZoomFactor;
                return {
                    x: (pageX - window.pageXOffset) * zoomFactor,
                    y: (pageY - window.pageYOffset) * zoomFactor
                };
            }

            function clipboardHandler(e) {
                // Create a menu and add commands with callbacks.
                try{
                    menu.commands.insertAt(0,new Windows.UI.Popups.UICommand("Copy to Clipboard", copyToClipboard));
                    
                    // We don't want to obscure content, so pass in the position representing the selection area.
                    // We registered command callbacks; no need to handle the menu completion event
                    menu.showAsync(pageToWinRT(e.pageX, e.pageY)).then(function (invokedCommand) {
                        if (invokedCommand === null) {
                            // The command is null if no command was invoked.
                            WinJS.log && WinJS.log("Context menu dismissed");
                            menu.commands.clear();
                        }
                    });
       
                    WinJS.log && WinJS.log("Context menu shown");
                } catch (e){
                    console.log(e);
                }
            }

            function copyToClipboard() {
                var _clipboard = Windows.ApplicationModel.DataTransfer.Clipboard,
                _dataTransfer = Windows.ApplicationModel.DataTransfer;
                var htmlContent = document.querySelector("#response").innerHTML;
                if (htmlContent.length > 0) {
                    var htmlContentFormated =
                        _dataTransfer.HtmlFormatHelper.createHtmlFormat(htmlContent);

                    var dataPackage = new _dataTransfer.DataPackage();
                    dataPackage.setHtmlFormat(htmlContentFormated);
                    dataPackage.setText(htmlContent);

                    _clipboard.setContent(dataPackage);
                }
            }
        
})();